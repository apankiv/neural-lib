import argparse
import re
import numpy as np

from os import path
from network.trainer import Trainer
from network.dataset import DataSet


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-m", "--model", default="", type=str, help="Path to directory storing checkpointed model.")
    parser.add_argument("-i", "--inputs", default="./dataset/inputs_small", type=str, help="Path to directory storing input images.")
    parser.add_argument("-t", "--targets", default="./dataset/targets_small", type=str, help="Path to directory storing target images.")
    parser.add_argument("-d", "--depth", default=4, type=int, help="Depth parameter. Default: 4.")
    parser.add_argument("-e", "--epochs", default=200, type=int, help="Epoch limit. Default: 200.")
    parser.add_argument("-l", "--limit", default=32, type=int, help="Batch size (in single images) limit. Default: 32.")

    args = parser.parse_args()

    image_dimensions = (args.depth, 280, 400, 1) if args.depth != 1 else (1, 560, 800, 1)

    def sf_inputs(array, pointer):
        return np.reshape(np.multiply(np.array([DataSet.load(file, image_dimensions[-3:]) for file in array[pointer:pointer+args.depth]], dtype=np.float32), 1.0 / 255.0), image_dimensions)

    def sf_targets(array, pointer):
        return np.reshape(np.multiply(np.array([DataSet.load(file, image_dimensions[-3:]) for file in array[pointer:pointer+args.depth]], dtype=np.float32), 1000.0), image_dimensions)

    filename_lambda = lambda name: name.split("/")[-1]

    dataset = DataSet(
        batch_size=(args.limit // args.depth * args.depth),
        data_dir=path.realpath(args.inputs),
        target_dir=path.realpath(args.targets),
        single_image_shape=image_dimensions[-3:],
        sorting_function=(lambda x: (int(re.split('[\W_]+', filename_lambda(x))[2]), int(re.split('[\W_]+', filename_lambda(x))[3]))),
        selector_function_inputs=sf_inputs,
        selector_function_targets=sf_targets,
        store_in_ram=False
    )

    trainer = Trainer(args.model, dataset, image_dimensions, args.epochs, args.limit)

    trainer.train()
