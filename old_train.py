import io
import os
import time
import gc
import re
import tensorflow as tf
import numpy as np
import datetime
import argparse
import matplotlib

from network.network3d import Network3D
from network.network2d import Network2D
from network.dataset import DataSet

matplotlib.use('Agg')
import matplotlib.pyplot as plt

timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")


def draw_results(test_inputs, test_targets, test_segmentation, test_accuracy, network, batch_num, n_examples_to_plot, data_shape):
    test_segmentation = np.reshape(test_segmentation, [data_shape[0], *data_shape[-3:-1]])
    test_inputs = np.reshape(test_inputs, [data_shape[0], *data_shape[-3:-1]])
    test_targets = np.reshape(test_targets, [data_shape[0], *data_shape[-3:-1]])

    fig, axs = plt.subplots(4, n_examples_to_plot, figsize=(n_examples_to_plot * 3, 10))
    fig.suptitle("Accuracy: {}, {}".format(test_accuracy, network.description), fontsize=20)

    for example_i in range(n_examples_to_plot):
        axs[0][example_i].imshow(test_inputs[example_i], cmap='gray')
        axs[1][example_i].imshow(test_targets[example_i], cmap='gray')
        axs[2][example_i].imshow(
            np.reshape(test_segmentation[example_i], [*data_shape[-3:-1]]),
            cmap='gray')

        test_image_thresholded = np.array(
            [0 if x < 0.8 else 255 for x in test_segmentation[example_i].flatten()])
        axs[3][example_i].imshow(
            np.reshape(test_image_thresholded, [*data_shape[-3:-1]]),
            cmap='gray')

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    image_plots_directory = 'image_plots/'

    if not os.path.exists(image_plots_directory):
            os.makedirs(image_plots_directory)

    plt.savefig('{}/figure{}.png'.format(image_plots_directory, batch_num))
    # return buf

def train(arguments):
    image_dimensions = (arguments.depth, 280, 400, 1) if arguments.depth != 1 else (560, 800, 1)

    def sf_inputs(array, pointer):
        # return array[pointer:pointer+arguments.depth]
        return np.reshape(np.multiply(np.array([DataSet.load(file) for file in array[pointer:pointer+arguments.depth]], dtype=np.float32), 1.0 / 255.0), image_dimensions)
        # return np.reshape(np.multiply(np.array(array[pointer:pointer+arguments.depth], dtype=np.float32), 1.0 / 255.0), image_dimensions)

    def sf_targets(array, pointer):
        # return array[pointer:pointer+arguments.depth]
        return np.reshape(np.multiply(np.array([DataSet.load(file) for file in array[pointer:pointer+arguments.depth]], dtype=np.float32), 1000.0), image_dimensions)
        # return np.reshape(np.multiply(np.array(array[pointer:pointer+arguments.depth], dtype=np.float32), 1000.0), image_dimensions)

    filename_lambda = lambda name: name.split("/")[-1]

    dataset = DataSet(
        batch_size=(arguments.limit // arguments.depth * arguments.depth),
        data_dir=os.path.realpath(arguments.inputs),
        target_dir=os.path.realpath(arguments.targets),
        sorting_function=(lambda x: (int(re.split('[\W_]+', filename_lambda(x))[2]), int(re.split('[\W_]+', filename_lambda(x))[3]))),
        selector_function_inputs=sf_inputs,
        selector_function_targets=sf_targets,
        store_in_ram=False
    )

    network = Network2D() if arguments.depth == 1 else Network3D(image_dimensions=image_dimensions)


    # create directory for saving models
    os.makedirs(os.path.join('save', network.description, timestamp))

    with tf.Session() as sess:
        saver = tf.train.Saver(tf.global_variables(), max_to_keep=None)
        checkpoint = tf.train.get_checkpoint_state(arguments.model)

        start = 0

        if checkpoint and checkpoint.model_checkpoint_path:
            print('Restoring model: {}'.format(checkpoint.model_checkpoint_path))
            saver.restore(sess, checkpoint.model_checkpoint_path)
            start = int(re.split('[\-]', checkpoint.model_checkpoint_path)[-1])
        else:
            print('No model found in {}. Learning from scratch.'.format(arguments.model))
            sess.run(tf.global_variables_initializer())

        print("Started session")

        summary_writer = tf.summary.FileWriter('{}/{}-{}'.format('logs', network.description, timestamp),
                                               graph=tf.get_default_graph())

        test_accuracies = []
        # Fit all training data
        n_epochs = arguments.epochs

        global_start = time.time()

        for epoch_i in range(start // dataset.num_batches_in_epoch(), n_epochs):
            for batch_i in range(dataset.num_batches_in_epoch()):
                batch_num = epoch_i * dataset.num_batches_in_epoch() + batch_i + 1

                start = time.time()
                batch_inputs, batch_targets = dataset.next_batch()

                cost, _ = sess.run([network.cost, network.train_op],
                                   feed_dict={network.inputs: batch_inputs,
                                              network.targets: batch_targets,
                                              network.is_training: True})
                end = time.time()
                print('{}/{}, epoch: {}, cost: {}, batch time: {}'.format(batch_num,
                                                                          n_epochs * dataset.num_batches_in_epoch(),
                                                                          epoch_i, cost, end - start))

                if batch_num % 100 == 0 or batch_num == n_epochs * dataset.num_batches_in_epoch():
                    test_inputs, test_targets = dataset.test_set

                    step = dataset.batch_size
                    count = len(test_inputs) // dataset.batch_size
                    partial_accuracies = []

                    for i in range(count):
                        current_test_inputs = np.reshape(np.array(test_inputs[i*step:i*step+step], dtype=np.float32), (-1, *image_dimensions))
                        current_test_targets = np.reshape(np.array(test_targets[i*step:i*step+step], dtype=np.float32), (-1, *image_dimensions))
                        current_test_inputs = np.multiply(current_test_inputs, 1.0 / 255.0)
                        current_test_targets = np.multiply(current_test_targets, 1000.0)

                        print("Testing iteration {}: subset {} - {}".format(i+1, i*step, i*step+step))

                        summary, test_accuracy = sess.run([network.summaries, network.accuracy],
                                                          feed_dict={network.inputs: current_test_inputs,
                                                                     network.targets: current_test_targets,
                                                                     network.is_training: False})

                        partial_accuracies.append(test_accuracy)

                    test_accuracy = sum(partial_accuracies) / float(len(partial_accuracies))

                    print('Step {}, test accuracy: {}'.format(batch_num, test_accuracy))
                    test_accuracies.append((test_accuracy, batch_num))
                    print("Accuracies in time: ", [test_accuracies[x][0] for x in range(len(test_accuracies))])
                    max_acc = max(test_accuracies)
                    print("Best accuracy: {} in batch {}".format(max_acc[0], max_acc[1]))
                    print("Total time: {}".format(time.time() - global_start))

                    # Plot example reconstructions
                    n_examples = (12 // arguments.depth * arguments.depth)
                    test_inputs = np.reshape(np.array(test_inputs[:n_examples]), (-1, *image_dimensions))
                    test_targets = np.reshape(np.array(test_targets[:n_examples]), (-1, *image_dimensions))
                    test_inputs = np.multiply(test_inputs, 1.0 / 255.0)
                    test_targets = np.multiply(test_targets, 1000.0)

                    test_segmentation = sess.run(network.segmentation_result, feed_dict={ network.inputs: test_inputs })

                    # Prepare the plot
                    draw_results(test_inputs, test_targets, test_segmentation, test_accuracy, network,
                                                 batch_num, n_examples, (-1, *image_dimensions))

                    if test_accuracy >= max_acc[0]:
                        checkpoint_path = os.path.join('save', network.description, timestamp, 'model.ckpt')
                        saver.save(sess, checkpoint_path, global_step=batch_num)

                    del test_inputs
                    del test_targets

                del batch_inputs
                del batch_targets

                gc.collect()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-m", "--model", default="", type=str, help="Path to directory storing checkpointed model.")
    parser.add_argument("-i", "--inputs", default="./dataset/inputs_small", type=str, help="Path to directory storing input images.")
    parser.add_argument("-t", "--targets", default="./dataset/targets_small", type=str, help="Path to directory storing target images.")
    parser.add_argument("-d", "--depth", default=4, type=int, help="Depth parameter. Default: 4.")
    parser.add_argument("-e", "--epochs", default=200, type=int, help="Epoch limit. Default: 200.")
    parser.add_argument("-l", "--limit", default=32, type=int, help="Batch size (in single images) limit. Default: 32.")

    args = parser.parse_args()

    train(args)
