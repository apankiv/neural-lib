import datetime
import tensorflow as tf
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import io
import numpy as np

from os import makedirs, path
from network.network3d import Network3D


class Trainer:
    """
    Trainer class gathers training steps and makes it more of a "black box".
    """
    MAX_EXAMPLES_TO_PLOT = 12

    def __init__(self,
                 model_path,
                 dataset,
                 image_dimensions,
                 epochs,
                 batch_item_limit):
        # timestamp for this run
        self.timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

        # network object
        self.network = Network3D(image_dimensions=image_dimensions)

        # create directory for saving models
        makedirs(path.join('save', self.network.description, self.timestamp))

        # some local variables for the future
        self.saver = None
        self.checkpoint = None
        self.summary_writer = None
        self.session = None
        self.global_start = None
        self.test_accuracies = []
        self.model = model_path
        self.epochs = epochs
        self.dataset = dataset
        self.image_dimensions = image_dimensions


    def train(self):
        """
        Train method runs training. After every epoch runs test()
        """
        with tf.Session() as self.session:
            self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=None)
            self.checkpoint = tf.train.get_checkpoint_state(self.model)

            # iteration when we start learning
            # quite useful when restoring session
            starting_point = 0

            if self.checkpoint and self.checkpoint.model_checkpoint_path:
                print('Restoring model: {}'.format(checkpoint.model_checkpoint_path))
                saver.restore(self.session, checkpoint.model_checkpoint_path)
                starting_point = int(re.split('[\-]', checkpoint.model_checkpoint_path)[-1])
            else:
                print('No model found in {}. Learning from scratch.'.format(self.model))
                self.session.run(tf.global_variables_initializer())

            print("Started session")

            summary_writer = tf.summary.FileWriter('{}/{}-{}'.format('logs', self.network.description, self.timestamp),
                                                   graph=tf.get_default_graph())

            self.global_start = time.time()
            batch_num = 0

            for epoch in range(starting_point // self.dataset.num_batches_in_epoch(), self.epochs):
                for batch in range(self.dataset.num_batches_in_epoch()):
                    batch_num = epoch * self.dataset.num_batches_in_epoch() + batch + 1

                    start = time.time()
                    batch_inputs, batch_targets = self.dataset.next_batch()

                    cost, _ = self.session.run([self.network.cost, self.network.train_op],
                                       feed_dict={self.network.inputs: batch_inputs,
                                                  self.network.targets: batch_targets,
                                                  self.network.is_training: True})
                    end = time.time()

                    print('{}/{}, epoch: {}, cost: {}, batch time: {}'.format(batch_num, self.epochs * self.dataset.num_batches_in_epoch(), epoch, cost, end - start))

                self.test(batch_num)

    def test(self, batch_num):
        """
        This method goes though whole test set (which is not too big) and
        gathers information about results. It also calls draw_results()
        """
        test_inputs, test_targets = self.dataset.test_set

        step = self.dataset.batch_size
        test_iterations = len(test_inputs) // self.dataset.batch_size
        partial_accuracies = []

        # to output only "nice" results :)
        best_i = 0
        best_accuracy = 0

        for i in range(test_iterations):
            current_test_inputs, current_test_targets = self.dataset.next_test_batch(i * step)

            print("Testing iteration {}: subset {} - {}".format(i+1, i*step, i*step+step))

            summary, test_accuracy = self.session.run([self.network.summaries, self.network.accuracy],
                                              feed_dict={self.network.inputs: current_test_inputs,
                                                         self.network.targets: current_test_targets,
                                                         self.network.is_training: False})
            if best_accuracy < test_accuracy:
                best_accuracy = test_accuracy
                best_i = i

            partial_accuracies.append(test_accuracy)

        test_accuracy = sum(partial_accuracies) / float(len(partial_accuracies))
        print('Step {}, test accuracy: {}'.format(batch_num, test_accuracy))
        self.test_accuracies.append((test_accuracy, batch_num))
        print("Accuracies in time: ", [self.test_accuracies[x][0] for x in range(len(self.test_accuracies))])
        max_acc = max(self.test_accuracies)
        print("Best accuracy: {} in batch {}".format(max_acc[0], max_acc[1]))
        print("Total time: {}".format(time.time() - self.global_start))

        if test_accuracy >= max_acc[0]:
            checkpoint_path = path.join('save', self.network.description, self.timestamp, 'model.ckpt')
            self.saver.save(self.session, checkpoint_path, global_step=batch_num)

        self.draw_results(test_accuracy, batch_num, best_i)

    def draw_results(self, test_accuracy, batch_num, best_i):
        """
        Draws maximum MAX_EXAMPLES_TO_PLOT results. Layout (row by row):

        - original
        - ground truth
        - segmentation result (raw)
        - segmentation result (thresholded)
        """
        n_examples = self.MAX_EXAMPLES_TO_PLOT // self.image_dimensions[0] * self.image_dimensions[0]
        current_test_inputs, current_test_targets = self.dataset.next_test_batch(best_i)

        test_segmentation = self.session.run(self.network.segmentation_result, feed_dict={ self.network.inputs: current_test_inputs })

        test_segmentation = np.reshape(test_segmentation, [-1, *self.image_dimensions[-3:-1]])
        test_inputs = np.reshape(current_test_inputs, [-1, *self.image_dimensions[-3:-1]])
        test_targets = np.reshape(current_test_targets, [-1, *self.image_dimensions[-3:-1]])

        fig, axs = plt.subplots(4, n_examples, figsize=(n_examples * 3, 10))
        fig.suptitle("Accuracy: {}, {}".format(test_accuracy, self.network.description), fontsize=20)

        for example_index in range(n_examples):
            axs[0][example_index].imshow(test_inputs[example_index], cmap='gray')
            axs[1][example_index].imshow(test_targets[example_index], cmap='gray')
            axs[2][example_index].imshow(
                np.reshape(test_segmentation[example_index], [*self.image_dimensions[-3:-1]]),
                cmap='gray')

            # IMPORTANT: change threshold if needed :)
            test_image_thresholded = np.array([0 if x < 0.8 else 255 for x in test_segmentation[example_index].flatten()])
            axs[3][example_index].imshow(
                np.reshape(test_image_thresholded, [*self.image_dimensions[-3:-1]]),
                cmap='gray')

            buf = io.BytesIO()
            plt.savefig(buf, format='png')
            buf.seek(0)

        image_plots_directory = "./image_plots_{}/".format(self.timestamp)

        if not path.exists(image_plots_directory):
            makedirs(image_plots_directory)

        plt.savefig('{}/figure{}.png'.format(image_plots_directory, batch_num))
