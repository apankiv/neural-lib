import tensorflow as tf

from network.layers.conv2d import Conv2D
from network.layers.max_pool_2d import MaxPool2D


# TODO: get rid of it
class Network2D:
    def __init__(self, image_dimensions=(560, 800, 1), layers=None, skip_connections=True):
        self.IMAGE_DIMENSIONS = image_dimensions

        if layers is None:
            layers = [
              Conv2D(kernel_size=5, strides=[1, 1, 1, 1], output_channels=4, name='conv_1_1'),
              Conv2D(kernel_size=5, strides=[1, 1, 1, 1], output_channels=4, name='conv_1_2'),
              MaxPool2D(kernel_size=2, name='max_1', skip_connection=skip_connections),

              Conv2D(kernel_size=5, strides=[1, 2, 2, 1], output_channels=4, name='conv_2_1'),
              Conv2D(kernel_size=5, strides=[1, 1, 1, 1], output_channels=4, name='conv_2_2'),
              MaxPool2D(kernel_size=2, name='max_2', skip_connection=skip_connections),

              Conv2D(kernel_size=5, strides=[1, 1, 1, 1], output_channels=4, name='conv_3_1'),
              Conv2D(kernel_size=5, strides=[1, 1, 1, 1], output_channels=4, name='conv_3_2'),
              MaxPool2D(kernel_size=2, name='max_3')
            ]

        self.inputs = tf.placeholder(tf.float32, [None, *image_dimensions], name='inputs')
        self.targets = tf.placeholder(tf.float32, [None, *image_dimensions], name='targets')
        self.is_training = tf.placeholder_with_default(False, [], name='is_training')
        self.description = ""

        self.layers = {}

        list_of_images_norm = tf.map_fn(tf.image.per_image_standardization, self.inputs)
        net = tf.stack(list_of_images_norm)

        # ENCODER
        for layer in layers:
            self.layers[layer.name] = net = layer.create_layer(net)
            self.description += "{}".format(layer.get_description())

        print("Current input shape: ", net.get_shape())

        layers.reverse()
        Conv2D.reverse_global_variables()

        # DECODER
        for layer in layers:
            net = layer.create_layer_reversed(net, prev_layer=self.layers[layer.name])

        self.segmentation_result = tf.sigmoid(net)

        print('segmentation_result.shape: {}, targets.shape: {}'.format(self.segmentation_result.get_shape(),
                                                                        self.targets.get_shape()))

        # MSE loss
        self.cost = tf.sqrt(tf.reduce_mean(tf.square(self.segmentation_result - self.targets)))
        self.train_op = tf.train.AdamOptimizer().minimize(self.cost)

        with tf.name_scope('accuracy'):
            argmax_probs = tf.round(self.segmentation_result)  # 0x1
            correct_pred = tf.cast(tf.equal(argmax_probs, self.targets), tf.float32)
            self.accuracy = tf.reduce_mean(correct_pred)

            tf.summary.scalar('accuracy', self.accuracy)

        self.summaries = tf.summary.merge_all()
