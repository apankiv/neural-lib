import tensorflow as tf

from network.utils import get_incoming_shape
from network.layers.layer import Layer
from network.layers.libs.activations import leaky_relu


class Conv3D(Layer):
    layer_index = 0

    def __init__(self, kernel_size, strides, output_channels, name):
        self.kernel_size = kernel_size
        self.strides = strides
        self.output_channels = output_channels
        self.name = name
        self.input_shape = None
        self.input_depth = None
        self.encoder_matrix = None

    @staticmethod
    def reverse_global_variables():
        Conv3D.layer_index = 0

    def create_layer(self, input_data):
        self.input_shape = get_incoming_shape(input_data)
        print("Conv3D layer {} input_shape {}".format(self.layer_index, self.input_shape))
        number_of_input_channels = self.input_shape[-1]
        self.input_depth = self.input_shape[1]

        with tf.variable_scope('conv', reuse=False):
            encoder_matrix = tf.get_variable(
                'W{}'.format(self.name[-3:]),
                shape=(self.input_depth, self.kernel_size, self.kernel_size, number_of_input_channels, self.output_channels)
            )
            b = tf.Variable(tf.zeros([self.output_channels]))

        self.encoder_matrix = encoder_matrix
        Conv3D.layer_index += 1

        output = tf.nn.conv3d(
            input_data,
            encoder_matrix,
            strides=self.strides,
            padding='SAME'
        )
        output = leaky_relu(tf.add(tf.contrib.layers.batch_norm(output), b))
        print("Conv3D layer {} output_shape {}".format(self.layer_index, output.get_shape().as_list()))

        return output

    def create_layer_reversed(self, input_data, prev_layer=None):
        print("Reversed layer {} input_shape {}".format(self.layer_index, self.input_shape))
        with tf.variable_scope('conv', reuse=True):
            W = tf.get_variable('W{}'.format(self.name[-3:]))
            b = tf.Variable(tf.zeros([W.get_shape().as_list()[3]]))

        output = tf.nn.conv3d_transpose(
            input_data,
            W,
            tf.stack([
                tf.shape(input_data)[0],
                self.input_shape[1],
                self.input_shape[2],
                self.input_shape[3],
                self.input_shape[4]
            ]),
            strides=self.strides,
            padding='SAME'
        )

        Conv3D.layer_index += 1
        output.set_shape([
            None,
            self.input_shape[1],
            self.input_shape[2],
            self.input_shape[3],
            self.input_shape[4]
        ])
        output = leaky_relu(tf.add(tf.contrib.layers.batch_norm(output), b))
        print("Reversed layer {} output_shape {}".format(self.layer_index, output.get_shape().as_list()))

        return output

    def get_description(self):
        return "C{},{},{}".format(self.kernel_size, self.output_channels, self.strides[1])
