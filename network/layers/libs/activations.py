import tensorflow as tf


def leaky_relu(x, leak=0.2, name="l_relu"):
    with tf.variable_scope(name):
        f1 = 0.5 * (1.0 + leak)
        f2 = 0.5 * (1.0 - leak)

        return f1 * x + f2 * abs(x)
