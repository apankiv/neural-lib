import tensorflow as tf

from network.utils import get_incoming_shape, max_pool_3d, unpool_3d
from network.layers.layer import Layer


class MaxPool3D(Layer):
    def __init__(self, kernel_size, name, skip_connection=False):
        self.kernel_size = kernel_size
        self.name = name
        self.skip_connection = skip_connection

    def create_layer(self, input_data):
        input_shape = get_incoming_shape(input_data)

        print("Pool input_shape {}".format(input_shape))

        return max_pool_3d(input_data, [1, self.kernel_size, self.kernel_size])

    def create_layer_reversed(self, input_data, prev_layer=None):
        input_shape = get_incoming_shape(input_data)

        print("Reversed pool input_shape {}".format(input_shape))

        if self.skip_connection:
            input_data = tf.add(input_data, prev_layer)

        output = unpool_3d(input_data, [1, self.kernel_size, self.kernel_size])

        print("Reversed pool output_shape {}".format(output.get_shape().as_list()))

        return output

    def get_description(self):
        return "M{}".format(self.kernel_size)
