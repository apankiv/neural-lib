import tensorflow as tf

from network.utils import max_pool_2d, unpool_2d
from network.layers.layer import Layer


class MaxPool2D(Layer):
    def __init__(self, kernel_size, name, skip_connection=False):
        self.kernel_size = kernel_size
        self.name = name
        self.skip_connection = skip_connection

    def create_layer(self, input_data):
        return max_pool_2d(input_data, self.kernel_size)

    def create_layer_reversed(self, input_data, prev_layer=None):
        if self.skip_connection:
            input_data = tf.add(input_data, prev_layer)

        return unpool_2d(input_data, self.kernel_size)

    def get_description(self):
        return "M{}".format(self.kernel_size)
