import numpy as np
import re
import cv2

from math import ceil, floor
from os import listdir, path

class DataSet:
    """
    Class DataSet is designed to manage your dataset in more or less generic
    way.
    """
    def __init__(self,
                 batch_size,
                 data_dir='../dataset/inputs_small',
                 target_dir='../dataset/targets_small',
                 single_image_shape=None,
                 sorting_function=None,
                 selector_function_inputs=(lambda x, y: x[y]),
                 selector_function_targets=(lambda x, y: x[y]),
                 # and in case someone wants it in RAM (like I do)
                 store_in_ram=False):
        # size of batch
        self.batch_size = batch_size

        # selectors defined by user
        self.selector_function_inputs = selector_function_inputs
        self.selector_function_targets = selector_function_targets

        # get files list (optional) sort them
        self.all_inputs = DataSet.recursive_list(data_dir)

        if not target_dir is None:
            self.all_targets = DataSet.recursive_list(target_dir)

        if sorting_function is None:
            # If no sorting function given use default that will place them
            # somehow logically
            self.all_inputs = sorted(self.all_inputs)

            if not target_dir is None:
                self.all_targets = sorted(self.all_targets)
        else:
            # Otherwise use sorting function provided
            self.all_inputs = sorted(self.all_inputs, key=sorting_function)

            if not target_dir is None:
                self.all_targets = sorted(self.all_targets, key=sorting_function)

        # set data type to float32 (single precision)
        self.data_type = np.float32

        # train files for training, test files for testing
        self.train_inputs, self.test_inputs = self.train_valid_test_split(self.all_inputs)
        self.train_targets, self.test_targets = self.train_valid_test_split(self.all_targets)

        # init dataset pointer
        self.pointer = 0

        # memory-related trick
        self.batch_inputs = None
        self.batch_targets = None

        # Load dataset to ram if asked for
        if store_in_ram:
            if single_image_shape is None:
                raise IOError("You have to set single_image_shape in order to load data to RAM!")

            self.train_inputs = [DataSet.load(f, single_image_shape) for f in self.train_inputs]
            self.test_inputs = [DataSet.load(f, single_image_shape) for f in self.test_inputs]
            self.train_targets = [DataSet.load(f, single_image_shape) for f in self.train_targets]
            self.test_targets = [DataSet.load(f, single_image_shape) for f in self.test_targets]

    @staticmethod
    def train_valid_test_split(data, ratio=None):
        """
        Method to split dataset into two pieces so after each epoch there is
        test subset
        """
        if ratio is None:
            ratio = (.8, .2)

        n = len(data)
        return (
          data[:int(ceil(n * ratio[0]))],
          data[int(ceil(n * ratio[0])):]
        )

    @staticmethod
    def recursive_list(dirname):
        """
        Listing whole directory tree recursively
        """
        result = []

        for p in listdir(dirname):
            if path.isdir(path.join(dirname, p)):
                result += recursive_list(path.join(dirname, p))
            elif path.isfile(path.join(dirname, p)):
                result.append(path.join(dirname, p))

        return result

    @staticmethod
    def load(file_path, file_shape):
        """
        Loading single file. If file does not exist just zeros.
        """
        return np.array(cv2.imread(file_path, 0), dtype=np.float32) if path.isfile(file_path) else np.zeros(file_shape)

    def num_batches_in_epoch(self):
        """
        Simple method for calculation number of iterations per epoch depending
        on batch size.
        """
        return int(floor(len(self.train_inputs) // self.batch_size))

    def next_batch(self):
        """
        Method for getting next batch of data for training.
        """
        self.batch_inputs = []
        self.batch_targets = []

        for _ in range(self.batch_size):
            if self.pointer + self.batch_size > len(self.train_inputs):
                self.pointer = 0
            batch_input_item, batch_target_item = self.next_batch_item(self.pointer)
            self.batch_inputs.append(batch_input_item)
            self.batch_targets.append(batch_target_item)
            self.pointer += 1

        return self.batch_inputs, self.batch_targets

    def next_batch_item(self, pointer):
        """
        Method for getting single item in batch.
        """
        batch_input_item = self.selector_function_inputs(self.train_inputs, pointer)
        batch_target_item = self.selector_function_targets(self.train_targets, pointer)

        return batch_input_item, batch_target_item

    @property
    def test_set(self):
        return self.test_inputs, self.test_targets

    # Brand new section
    def next_test_batch(self, pointer):
        """
        Method for getting next batch of data for testing.
        """
        self.batch_inputs = []
        self.batch_targets = []

        for _ in range(self.batch_size):
            batch_input_item, batch_target_item = self.next_test_batch_item(pointer)
            self.batch_inputs.append(batch_input_item)
            self.batch_targets.append(batch_target_item)
            self.pointer += 1

        return self.batch_inputs, self.batch_targets

    def next_test_batch_item(self, pointer):
        """
        Method for getting single testing item in batch.
        """
        batch_input_item = self.selector_function_inputs(self.test_inputs, pointer)
        batch_target_item = self.selector_function_targets(self.test_targets, pointer)

        return batch_input_item, batch_target_item
