import tensorflow as tf
import numpy as np


def auto_format_padding(padding):
    padding = str.upper(padding)

    if padding in ['SAME', 'VALID']:
        return padding
    else:
        raise Exception("Unknown padding! Accepted values: 'same', 'valid'.")


def get_incoming_shape(incoming):
    if isinstance(incoming, tf.Tensor):
        return incoming.get_shape().as_list()
    elif type(incoming) in [np.array, list, tuple]:
        return np.shape(incoming)
    else:
        raise Exception("Invalid incoming layer.")


def auto_format_kernel_3d(strides):
    if isinstance(strides, int):
        return [1, 1, strides, strides, 1]
    elif isinstance(strides, (tuple, list)):
        if len(strides) == 3:
            return [1, strides[0], strides[1], strides[2], 1]
        elif len(strides) == 5:
            return strides
        else:
            raise Exception("strides length error: " + str(len(strides))
                            + ", only a length of 3 or 5 is supported.")
    else:
        raise Exception("strides format error: " + str(type(strides)))


def max_pool_3d(incoming, kernel_size, strides=None, padding='same', name="MaxPool3D"):
    input_shape = get_incoming_shape(incoming)
    assert len(input_shape) == 5, "Incoming Tensor shape must be 5-D"

    kernel = auto_format_kernel_3d(kernel_size)
    strides = auto_format_kernel_3d(strides) if strides else kernel
    padding = auto_format_padding(padding)

    with tf.name_scope(name) as scope:
        inference = tf.nn.max_pool3d(incoming, kernel, strides, padding)

        tf.add_to_collection(tf.GraphKeys.ACTIVATIONS, inference)

    inference.scope = scope

    return inference


def unpool_3d(incoming, kernel_size, name="UpSample3D"):
    input_shape = get_incoming_shape(incoming)

    assert len(input_shape) == 5, "Incoming Tensor shape must be 5-D"
    kernel = auto_format_kernel_3d(kernel_size)
    incoming = tf.reshape(incoming, [-1, *input_shape[2:5]])

    with tf.name_scope(name) as scope:
        inference = tf.image.resize_nearest_neighbor(
            incoming, size=input_shape[2:4] * tf.constant(kernel[2:4]))
        inference = tf.reshape(inference, [-1, input_shape[1], input_shape[2] * kernel[2], input_shape[3] * kernel[3], input_shape[4] * kernel[4]])
        inference.set_shape((None, input_shape[1], input_shape[2] * kernel[2],
                             input_shape[3] * kernel[3], input_shape[4] * kernel[4]))

    inference.scope = scope

    return inference


def auto_format_kernel_2d(strides):
    if isinstance(strides, int):
        return [1, strides, strides, 1]
    elif isinstance(strides, (tuple, list)):
        if len(strides) == 2:
            return [1, strides[0], strides[1], 1]
        elif len(strides) == 4:
            return [strides[0], strides[1], strides[2], strides[3]]
        else:
            raise Exception("strides length error: " + str(len(strides))
                            + ", only a length of 2 or 4 is supported.")
    else:
        raise Exception("strides format error: " + str(type(strides)))


def max_pool_2d(incoming, kernel_size, strides=None, padding='same',
                name="MaxPool2D"):
    input_shape = get_incoming_shape(incoming)
    assert len(input_shape) == 4, "Incoming Tensor shape must be 4-D"

    kernel = auto_format_kernel_2d(kernel_size)
    strides = auto_format_kernel_2d(strides) if strides else kernel
    padding = auto_format_padding(padding)

    with tf.name_scope(name) as scope:
        inference = tf.nn.max_pool(incoming, kernel, strides, padding)

        tf.add_to_collection(tf.GraphKeys.ACTIVATIONS, inference)

    inference.scope = scope

    return inference


def unpool_2d(incoming, kernel_size, name="UpSample2D"):
    input_shape = get_incoming_shape(incoming)
    assert len(input_shape) == 4, "Incoming Tensor shape must be 4-D"
    kernel = auto_format_kernel_2d(kernel_size)

    with tf.name_scope(name) as scope:
        inference = tf.image.resize_nearest_neighbor(
            incoming, size=input_shape[1:3] * tf.constant(kernel[1:3]))
        inference.set_shape((None, input_shape[1] * kernel[1],
                             input_shape[2] * kernel[2], None))

    inference.scope = scope

    return inference
