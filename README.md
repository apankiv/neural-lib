# Neural lib

Library (-ish) based on masters thesis (mine :>)

## Install requirements

To install required libraries just run:

```
pip3 install -r requirements.txt
```

## Arguments

In order to get list of arguments with description for training run:

```
python3 train.py -h
```

In order to get list of arguments with description for execution run:

```
python3 run.py -h
```

## Modules

All modules are mostly self-explanatory. General description follows:

### DataSet

Arguments:

* `batch_size` - size of single iteration batch
* `data_dir` - path to directory with original images
* `target_dir` - path to directory with ground truth
* `sorting_function` - function [comparator] to sort files (ref. `sorted` python3 function)
* `selector_function_inputs` [container, index] - function used to select single item from inputs
* `selector_function_targets` [container, index] - function used to select single item from targets

IMPORTANT!

`selector_function_inputs` and `selector_function_targets` should also implement transformations (if any)
needed for items. See examples in `train.py`.

### Network (2D/3D)

Arguments:

* `image_dimensions` - dimensions of single 2D/3D image. Tuple format: `(depth, height, width, channels)`
* `layers` - model
* `skip_connections` - model parameter [TODO: get rid of it, probably pointless]

## WIP

* clean `skip_connections`
