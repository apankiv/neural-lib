import argparse
import tensorflow as tf
import numpy as np
import cv2
import os

from network.network3d import Network3D
from network.network2d import Network2D
from network.dataset import DataSet

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inputs", default="./dataset/inputs_small", type=str, help="Path to directory storing input images.")
    parser.add_argument("-m", "--model", default="", type=str, help="Path to directory storing trained model.")
    parser.add_argument("-o", "--out", default="./tmp", type=str, help="Path to directory to store resulting image.")
    parser.add_argument("-t", "--threshold", default=0.4, type=float, help="Threshold value. Default: 0.4")
    parser.add_argument("-d", "--depth", default=4, type=int, help="Depth parameter. Default: 4.")
    parser.add_argument("-l", "--limit", default=32, type=int, help="Batch size (in single images) limit. Default: 32.")

    args = parser.parse_args()

    image_dimensions = (args.depth, 280, 400, 1) if args.depth != 1 else (args.depth, 560, 800, 1)

    network = Network3D(image_dimensions=image_dimensions)
    # dataset = DataSet(args.limit // args.depth * args.depth, args.inputs, None, False, depth=args.depth)

    def sf_inputs(array, pointer):
        return np.reshape(np.multiply(np.array([DataSet.load(file, *image_dimensions[-3]) for file in array[pointer:pointer+args.depth]], dtype=np.float32), 1.0 / 255.0), image_dimensions)

    dataset = DataSet(
        batch_size=(args.limit // args.depth * args.depth),
        data_dir=path.realpath(args.inputs),
        single_image_shape=image_dimensions[-3:],
        target_dir=None,
        sorting_function=(lambda x: (int(re.split('[\W_]+', filename_lambda(x))[2]), int(re.split('[\W_]+', filename_lambda(x))[3]))),
        selector_function_inputs=sf_inputs,
        selector_function_targets=sf_targets,
        store_in_ram=False
    )

    images = dataset.all_inputs

    checkpoint = args.model

    with tf.Session() as sess:
        saver = tf.train.Saver(tf.all_variables())
        ckpt = tf.train.get_checkpoint_state(checkpoint)

        if ckpt and ckpt.model_checkpoint_path:
            print('Restoring model: {}'.format(ckpt.model_checkpoint_path))
            saver.restore(sess, ckpt.model_checkpoint_path)
        else:
            raise IOError('No model found in {}.'.format(checkpoint))

        while i - args.depth < len(images):
            segmentation = sess.run(network.segmentation_result, feed_dict={network.inputs: np.reshape(sf_inputs(images, i * args.depth), image_dimensions)})

            for j in range(args.depth):
                image = np.reshape(segmentation[i * args.depth + j], image_dimensions[-3:-1])
                image_threshold = np.array([0 if x < args.threshold else 255 for x in image.flatten()])
                image = np.reshape(image_threshold, image_dimensions[-3:-1])
                cv2.imwrite(os.path.join(args.out, names[i * args.depth + j]), image)

            i += args.depth
